# Cardiac_Functoin_Analysis

This Readme File contain all the step by step to reproduce the information to analyze the cardiac function on the frequency domain. Thise project serves as complementary material to reproduce and identify the potential work in terms of establishes a better indicator of the cardiac function instead of triditional and limited current method as the LVEF.


## Getting started


* This url, contains the local repository provided by the project :"Estimation of cardiovascular risk integrating Industry 4.0 technologies for the management, processing, and analysis of medical information with code 82335 from FCTeI of the call No. 890 of 2020 of MinCiencia" 

* This url, contain the codes provided to reproduce the research and interpretability. Each code is renamed to understand the processing from the initial segmentation until it reach the clustering technique, UMAP projection and the figures presented in the paper.

* The Local repositroy contains a .csv file with the respective labels of the cases, there cardiovascular diagnosis and the non-cardiovascular diagnosis. In this .csv at the end of the file, contains the url directoin tyo the folder with the cases already interpolated, downsampling and cropped withoput metadata. This video is a 4-apical chamber video. Each frames of the video is 112 X 112 pixel dimension. This with the aim to be compared with other public databases like EchoNet-dynamics.

* Link to the local repository: https://unaledu-my.sharepoint.com/:u:/g/personal/proyectocardio890fm_bog_unal_edu_co/EacGQV-PWWRAllVAsNTAWRkBw-zgGKWk5lJVWBqFBCa0Og?e=5j1Zjm

* For replicated this analysis, download the database, change the paths of the local codes and then starts with area detection code, pass to interpolation frequency and finally with cluster analysis.

## Authors and acknowledgment
This work was funded by "Estimation of cardiovascular risk integrating Industry 4.0 technologies for the management, processing, and analysis of medical information" with code 82335 from FCTeI of the call No. 890 of 2020 of MinCiencia

## Project status
This public data and alysis were opened for been acepted in the next biomedical conference called. "27th INTERNATIONAL CONFERENCE ON MEDICAL IMAGE COMPUTING AND COMPUTER ASSISTED INTERVENTION" MICCAI 2024, with the paper ID 3315. 
